package c07

// copyright 2012-13 Jon Kerridge
// Let's Do It In Parallel

import org.jcsp.lang.*
import org.jcsp.groovy.*



class Client implements CSProcess{  
	
  def ChannelInput receiveChannel
  def ChannelOutput requestChannel
  def clientNumber   
  def selectList = [ ] 
     
  // client lists for test
  def ExpectedListCl0 = [10,20,30,40,50,60,70,180,90,100]
  def ExpectedListCl1 = [110,120,130,40,150,160,170,180,190,200]
  def checklist0 = [ ]
   
  void run () {
    def iterations = selectList.size
    println "Client $clientNumber has $iterations values in $selectList"
	
    for ( i in 0 ..< iterations) {
      def key = selectList[i]
      requestChannel.write(key)
      def v = receiveChannel.read()
	  checklist0[i] = v
	  println "checklist for $clientNumber : $checklist0"
    }
	
	if (checklist0.equals(ExpectedListCl0) || checklist0.equals(ExpectedListCl1))
	{println "All values are in the correct order for Client $clientNumber"}
	else{ println" Values are not in the correct order for Client $clientNumber" }	
    println "Client $clientNumber has finished"
  }
}
