package c09

import org.jcsp.lang.*
import org.jcsp.groovy.*
import org.jcsp.groovy.plugAndPlay.*

class Validator implements CSProcess{

	def ChannelInput inChannel
	def ChannelOutput outChannel
	def data = 99
	void run() {
		while (true) {
			def e = inChannel.read()
			def missed = ( e.data - data - 1) 
			if(missed != e.missed ){
				println "Event with data $e.data, missed missmatch $e.missed, correct is $missed"
				data =  e.data
			}
			else if (missed == e.missed){
				println "Event with data $e.data, didn't missmatch"
				data =  e.data
			}
			outChannel.write(e)
		}
	}
}
