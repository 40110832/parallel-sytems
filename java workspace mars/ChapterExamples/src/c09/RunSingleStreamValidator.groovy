package c09

import org.jcsp.lang.*
import org.jcsp.groovy.*
import org.jcsp.groovy.plugAndPlay.*

def eg2h = Channel.one2one()
def h2udd = Channel.one2one()
def udd2val = Channel.one2one()
def val2prn = Channel.one2one()

def eventTestList = [ 
      new EventGenerator ( source: 1, 
                           initialValue: 100, 
                           iterations: 100, 
                           outChannel: eg2h.out(), 
                           minTime: 100, 
                           maxTime: 200 ),
					   
      new EventHandler ( inChannel: eg2h.in(), 
                         outChannel: h2udd.out() ),
					 
      new UniformlyDistributedDelay ( inChannel:h2udd.in(), 
                                      outChannel: udd2val.out(), 
                                      minTime: 500, 
                                      maxTime: 1000 ), 
								  
	  new Validator ( inChannel: udd2val.in(),
						 	outChannel: val2prn.out() ),
					 
      new GPrint ( inChannel: val2prn.in(),
    		        heading : "Event Output",
    		        delay: 0)
      ]

new PAR ( eventTestList ).run()