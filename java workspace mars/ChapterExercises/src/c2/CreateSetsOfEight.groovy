package c2

import org.jcsp.lang.*

class CreateSetsOfEight implements CSProcess{
	
	def ChannelInput inChannel
	def outList = []
	def outListtest = []
	void run(){
		def outList = []
		def v = inChannel.read()
		while (v != -1){
			for ( i in 0 .. 7 ) {
				outList << v
				outListtest = outList
				v = inChannel.read()
			}
			println " Eight Object is ${outList}"
			outList = [];
		}
		println "Finished"
	
	}
}