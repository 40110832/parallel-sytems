package c2


import org.jcsp.lang.*
import org.jcsp.groovy.*

class ThreeTo8Test extends GroovyTestCase {

	public void test() {

		One2OneChannel connect1 = Channel.createOne2One()
		One2OneChannel connect2 = Channel.createOne2One()


		def GenerateSetsOfThreetest = new GenerateSetsOfThree ( outChannel: connect1.out() )
		def ListToStreamtest = new ListToStream ( inChannel: connect1.in(),outChannel: connect2.out() )
		def CreateSetsOfEighttest = new CreateSetsOfEight ( inChannel: connect2.in() )


		def testList = [GenerateSetsOfThreetest, ListToStreamtest, CreateSetsOfEighttest]
		new PAR (testList).run()

		def expected = [17, 18, 19, 20, 21, 22, 23, 24]
		def actual = CreateSetsOfEighttest.outListtest
        

		assertTrue(expected == actual)
	}
}