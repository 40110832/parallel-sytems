package c5

import org.jcsp.lang.*
import org.jcsp.groovy.*

class Controller implements CSProcess{
	ChannelInput newFactorFromInterface
	ChannelOutput suspend
	ChannelOutput injector

	public void run() {
		while (true) {
			def newfactor = newFactorFromInterface.read()
			println "send signal to suspend"
			suspend.write (0)
			println "send new factor"
			injector.write (Integer.parseInt(newfactor))
		}
	}
}