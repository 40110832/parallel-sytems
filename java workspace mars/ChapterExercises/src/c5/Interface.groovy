package c5

import java.awt.*
import org.jcsp.groovy.*
import org.jcsp.awt.*
import org.jcsp.lang.*
import org.jcsp.util.*

class Interface implements CSProcess{
	ChannelInput currentFactor
	ChannelInput data
	ChannelOutput newFactor
	
	void run(){
		
		def frame = new ActiveClosingFrame("Scaler Interface")
		def mainFrame = frame.getActiveFrame()		
		def label = new ActiveLabel (currentFactor)
		def newFactor = new ActiveTextEnterField(null,newFactor,"")
		def text = new ActiveTextArea(data, null, "", 0, 0, ActiveTextArea.SCROLLBARS_VERTICAL_ONLY)		
		def labelContainer = new Container()
		
		label.setAlignment(label.CENTER)
		labelContainer.setLayout(new GridLayout(3,1))
		labelContainer.add(label)
		labelContainer.add(newFactor.getActiveTextField())
		labelContainer.add(text)
		
		mainFrame.add(labelContainer)		
		mainFrame.pack()
		mainFrame.setVisible(true)	
		
		def network = [frame, newFactor, text, label]
		new PAR(network).run()
	}
}