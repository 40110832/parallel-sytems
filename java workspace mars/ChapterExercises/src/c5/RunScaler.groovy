package c5
 
import org.jcsp.lang.*
import org.jcsp.groovy.*
import org.jcsp.groovy.plugAndPlay.*

def data = Channel.createOne2One()
def timedData = Channel.createOne2One()
def scaledData = Channel.createOne2One()
def oldScale = Channel.createOne2One()
def newScale = Channel.createOne2One()
def pause = Channel.createOne2One()
def interfaceFactor = Channel.createOne2One()
def network = [ 
	new GNumbers ( outChannel: data.out() ),
	new GFixedDelay ( 
		delay: 1000,
		inChannel: data.in(),
		outChannel: timedData.out() ),
	new Scale ( 
		inChannel: timedData.in(),
		outChannel: scaledData.out(),
		factor: oldScale.out(),
		suspend: pause.in(),
		injector: newScale.in(),
		scaling: 2 ),
	new Interface (
		currentFactor: oldScale.in(),
		data: scaledData.in(),
		newFactor:interfaceFactor.out()
	),
	new Controller(
		newFactorFromInterface: interfaceFactor.in(),
		suspend: pause.out(),
		injector: newScale.out()
	)
]
new PAR ( network ).run()